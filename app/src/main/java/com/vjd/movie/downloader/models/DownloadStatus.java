package com.vjd.movie.downloader.models;

import androidx.room.Entity;

//import com.moviedownloader.videodownloader.helpers.DownloadUtils;


@Entity
public class DownloadStatus {
//    // Constants
//    public static final int STATUS_PAUSED = 0;
//    public static final int STATUS_RESUMED = 1;
//    public static final int STATUS_COMPLETED = 2;
//
//    @PrimaryKey(autoGenerate = true)
//    private long id;
//    private String title;
//    private String filePath;
//    private int downloadStatus;
//    private int progress;
//    private String sourceName;
//    private DownloadUtils.MimeType mimeType;
//
//    public DownloadStatus(long id, String title, String filePath, DownloadUtils.MimeType mimeType, String sourceName, int downloadStatus, int progress) {
//        this.id = id;
//        this.title = title;
//        this.filePath = filePath;
//        this.sourceName = sourceName;
//        this.downloadStatus = downloadStatus;
//        this.progress = progress;
//        this.mimeType = mimeType;
//    }
//
//    public int getDownloadStatus() {
//        return downloadStatus;
//    }
//
//    public void setDownloadStatus(int downloadStatus) {
//        this.downloadStatus = downloadStatus;
//    }
//
//    public int getProgress() {
//        return progress;
//    }
//
//    public DownloadUtils.MimeType getMimeType() {
//        return mimeType;
//    }
//
//    public void setMimeType(DownloadUtils.MimeType mimeType) {
//        this.mimeType = mimeType;
//    }
//
//    public void setProgress(int progress) {
//        this.progress = progress;
//    }
//
//    // Bundling them under one definition
//    @Retention(RetentionPolicy.SOURCE)
//    @IntDef({STATUS_PAUSED, STATUS_RESUMED, STATUS_COMPLETED})
//    public @interface DownloadStatusDef {
//    }
//
//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public String getFilePath() {
//        return filePath;
//    }
//
//    public void setFilePath(String filePath) {
//        this.filePath = filePath;
//    }
//
//    public String getSourceName() {
//        return sourceName;
//    }
//
//    public void setSourceName(String sourceName) {
//        this.sourceName = sourceName;
//    }
}
