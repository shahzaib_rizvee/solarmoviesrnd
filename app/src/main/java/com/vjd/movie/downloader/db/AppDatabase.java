package com.vjd.movie.downloader.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.vjd.movie.downloader.db.converters.SongsConverter;
import com.vjd.movie.downloader.db.converters.StringListConverter;
import com.vjd.movie.downloader.db.dao.AlbumDao;
import com.vjd.movie.downloader.db.dao.SongDao;
import com.vjd.movie.downloader.db.dao.URLStatsDao;
import com.vjd.movie.downloader.models.Album;
import com.vjd.movie.downloader.models.Movie;
import com.vjd.movie.downloader.models.URLStats;

@Database(
        entities = {
                Album.class, URLStats.class, Movie.class
        },
        version = 3,
        exportSchema = false
)
@TypeConverters(
        {
                //IntegerListConverter.class,
                SongsConverter.class,
                StringListConverter.class
        }
)
public abstract class AppDatabase extends RoomDatabase {
    public abstract AlbumDao getAlbumDao();

    public abstract URLStatsDao getURLStatsDao();

    public abstract SongDao getSongDao();
}