package com.vjd.movie.downloader.models;

public class MovieDetails {

    private MovieBasic movieBasic;
    private String generes,directors,description,runtime,actors,release;

    public MovieDetails(MovieBasic movieBasic, String generes, String directors, String description, String runtime, String actors, String release) {
        this.movieBasic = movieBasic;
        this.generes = generes;
        this.directors = directors;
        this.description = description;
        this.runtime = runtime;
        this.actors = actors;
        this.release = release;
    }

    public MovieBasic getMovieBasic() {
        return movieBasic;
    }

    public void setMovieBasic(MovieBasic movieBasic) {
        this.movieBasic = movieBasic;
    }

    public String getGeneres() {
        return generes;
    }

    public void setGeneres(String generes) {
        this.generes = generes;
    }

    public String getDirectors() {
        return directors;
    }

    public void setDirectors(String directors) {
        this.directors = directors;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

}
