package com.vjd.movie.downloader;

public class Constants {
    public static String URL_BASE_MOVIES = "https://azm.to";
    public static String URL_HOMEPAGE = "/home";
    public static String URL_BASE_THUMBNAIL = "https://ww1.solarmovie.cr";

    //    Keys for Intents
    public static final String KEY_MUSIC_TYPE = "MUSIC_TYPE_KEY";
    public static final String GENRES_URL = "GENRES_URL";
    public static final String GENRES_KEY = "GENRES_KEY";
    public static final String KEY_ALBUM_ID = "ALBUM_ID_KEY";

    public static final int FAV_LIST_AD_INTERVAL = 4;
    public static final int DOWNLOAD_LIST_AD_INTERVAL = 4;
    public static final int SEARCH_LIST_AD_INTERVAL = 4;
    public static final int BROWSE_LIST_AD_INTERVAL = 4;


    public static final String SEARCH_QUERY = "search-query";
    public static final String SEARCH_MOVIE = "Search";
    public static final String ACTION_MOVIE = "action";
    public static final String ADULT_MOVIE = "adult-movie";

    public final static String LATEST_MOVIES = "latest-movies";
    public final static String FEATURED_MOVIES = "featured-movies";
    public static final String TAMIL_MOVIE = "tamil-movie";
    public final static String TELUGU_MOVIES_2019 = "telugu-movies-2019";
    public final static String MALAYALAM_MOVIES = "malayalam-movi";


    public final static String PUNJABI_MOVIE = "punjabi-movie";
    public final static String PUNJABI_ALBUMS = "punjabi-movie";

    public final static String BOLLYWOOD_MOVIE_2019 = "bollywood-movie-2019";
    public final static String POP_REMIX_ALBUMS = "bollywood-movie-2019";

    public final static String HOLLYWOOD_MOVIE_2019 = "hollywood-movie-2019";

    public final static String POP_REMIX = "pop-remix";

    public final static String PAKISTANI = "punjabi-albums";
    public final static String PAKISTANI_ALBUMS = "punjabi-albums";
    public final static String PAKISTANI_SINGLES = "punjabi-albums";
    public final static String PUNJABI = "punjabi";

    public final static String GHAZALS = "punjabi-albums";

    public final static String WEDDING = "wedding";
    public final static String INSTRUMENTAL = "instrumental";
    public final static String CHARTS_WEEKLY = "charts-weekly";
    public final static String CHARTS_MONTHLY = "charts-monthly";

    public static String from = "ANY";


    public final static int CODE_ACTION = 3050;
    public final static int CODE_SEARCH = 3051;

    public final static int CODE_LATEST_MOVIES = 2999;
    public final static int CODE_FEATURED_MOVIES = 3000;
    public final static int CODE_TELUGU_MOVIES_2019 = 3001;
    public final static int CODE_MALAYALAM_MOVIES = 3002;

    public final static int CODE_POP_REMIX = 1000;
    public final static int CODE_BOLLYWOOD_MOVIE_2019 = 1001;
    public final static int CODE_POP_REMIX_ALBUMSS = 1002;

    public final static int CODE_PAKISTANI = 2000;
    public final static int CODE_PAKISTANI_ALBUMS = 2002;
    public final static int CODE_PAKISTANI_SINGLES = 2001;

    public final static int CODE_PUNJABI = 4000;
    public final static int CODE_PUNJABI_ALBUMS = 4002;
    public final static int CODE_PUNJABI_MOVIE = 4001;
    public final static int CODE_GHAZALS = 5000;
    public final static int CODE_HOLLYWOOD_MOVIE_2019 = 6000;
    public final static int CODE_WEDDING = 7000;
    public final static int CODE_INSTRUMENTAL = 8000;
    public final static int CODE_CHARTS_WEEKLY = 9001;
    public final static int CODE_CHARTS_MONTHLY = 9002;
    public final static int CODE_TAMIL_MOVIE = 10000;


    public final static String URL_SEARCH_MOVIE = "/?s=";
    public final static String URL_ACTION_MOVIE = "/tag/action/";
    public final static String URL_ADULT_MOVIE = "/category/adult-movie/";

    public final static String URL_LATEST_MOVIES = URL_BASE_MOVIES;
    public final static String URL_FEATURED_MOVIE = "/category/featured";
    public final static String URL_TAMIL_MOVIE = "/category/tamil-movie";
    public final static String URL_TELUGU_MOVIES_2019 = "/category/telugu-movies-2019";
    public final static String URL_MALAYALAM_MOVIES = "/category/malayalam-movie";
    //   public final static String URL_TELUGU_MOVIES_2019="https://songspk.mobi/browse/bollywood-singles";

    public final static String URL_PUNJABI_ALBUMS = "/category/punjabi-movie";
    public final static String URL_PUNJABI_MOVIES = "/category/punjabi-movie";

    public final static String URL_BOLLYWOOD_MOVIES_2019 = "/category/bollywood-movie-2019";
    public final static String URL_POP_REMIX_ALBUMS = "/category/bollywood-movie-2019";

    public final static String URL_HOLLYWOOD_MOVIES_2019 = "/category/hollywood-movie-2019";


    public final static String URL_POP_REMIX = "https://songspk.mobi/songs/pop-remix";


    public final static String URL_PUNJABI = "https://songspk.mobi/songs/punjabi";

    public final static String URL_PAKISTANI = "https://songspk.mobi/songs/pakistani";
    public final static String URL_PAKISTANI_ALBUMS = "https://songspk.mobi/browse/pakistani-albums";
    public final static String URL_PAKISTANI_SINGLES = "https://songspk.mobi/browse/pakistani-singles";

    public final static String URL_GHAZALS = "https://songspk.mobi/browse/ghazals";
    public final static String URL_WEDDING = "https://songspk.mobi/browse/wedding";
    public final static String URL_INSTRUMENTAL = "https://songspk.mobi/browse/instrumental";

    public final static String URL_CHARTS_WEEKLY = "https://songspk.mobi/charts/weekly";
    public final static String URL_CHARTS_MONTHLY = "https://songspk.mobi/charts/monthly";


    public static final int LIMIT_HOME_PAGE_MALAYALAM_MOVI = 24;
    public static final int LIMIT_HOME_PAGE_TELUGU_MOVIES_2019 = 24;
    public static final int LIMIT_HOME_PAGE_FEATURED_MOVIES_2019 = 24;
    public static final int LIMIT_HOME_PAGE_TAMIL_MOVIE = 24;
    public static final int LIMIT_HOME_PAGE_HOLLYWOOD_MOVIE_2019 = 5;
    public static final int LIMIT_HOME_PAGE_BOLLYWOOD_MOVIE_2019 = 4;
    public static final int LIMIT_HOME_PAGE_PUNJABI_ALBUMS = 4;
    public static final int LIMIT_PAGE_SIZE_LONG = 16;
    public static final int LIMIT_PAGE_SIZE_MID = 16;


    public final static String VIDEO_LINK = "videoLink";

    public static final String LOCAL_VIDEOS = "Videos";
    public static final String MOVIES_FOLDER = "AllMoviesDownloader";
    public static final String VIDEO = "VIDEO";


}
