package com.vjd.movie.downloader.adapters;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vjd.movie.downloader.IAlbumListener;
import com.vjd.movie.downloader.models.Movie;
import com.vjd.movie.downloader.models.MovieBasic;

import java.util.List;

public abstract class MoviesBaseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    IAlbumListener iAlbumListener;
    List<MovieBasic> movieBasics;


    public void setAlbumListener(IAlbumListener iAlbumListener) {
        this.iAlbumListener = iAlbumListener;
    }

    public void setAlbumList(List<MovieBasic> movieList) {
        this.movieBasics = movieList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public abstract RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType);

    @Override
    public abstract void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position);

    @Override
    public int getItemCount() {
        return movieBasics == null ? 0 : movieBasics.size();
    }


}
