package com.vjd.movie.downloader.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class URLStats {

    @PrimaryKey
    @NonNull
    String url;
    long lastCalledAt;
    String date;
    int updateInterval;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getLastCalledAt() {
        return lastCalledAt;
    }

    public void setLastCalledAt(long lastCalledAt) {
        this.lastCalledAt = lastCalledAt;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getUpdateInterval() {
        return updateInterval;
    }

    public void setUpdateInterval(int updateInterval) {
        this.updateInterval = updateInterval;
    }
}
