package com.vjd.movie.downloader.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import com.vjd.movie.downloader.models.Album;
import com.vjd.movie.downloader.models.AlbumWithSongs;

import java.util.List;


@Dao
public abstract class AlbumDao extends BaseDao<Album> {

    @Query("select * from Album where tags like '%' || :catetgory || '%' Order by id desc limit :limit")
    public abstract LiveData<List<Album>> getAlbumsList(String catetgory, int limit);

    @Query("DELETE FROM Album WHERE tags like '%' ||  :category || '%' ")
    public abstract int getDeleteSearchList(String category);

    @Query("SELECT * FROM Album WHERE id = :id ")
    public abstract Album getAlbumById(int id);

    @Query("SELECT * FROM Album WHERE id = :id ")
    public abstract LiveData<Album> getAlbumByIdLiveData(int id);

    @Query("SELECT * FROM Album WHERE id = :id")
    @Transaction
    public abstract LiveData<AlbumWithSongs> getAlbumWithSongsLiveData(int id);

    @Query("SELECT * FROM Album WHERE id = :id")
    @Transaction
    public abstract AlbumWithSongs getAlbumWithSongs(int id);

}
