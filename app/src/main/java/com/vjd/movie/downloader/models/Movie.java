package com.vjd.movie.downloader.models;


import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(indices = {@Index("movieId"), @Index("albumId")}, foreignKeys = @ForeignKey(entity = Album.class,
        parentColumns = "id",
        childColumns = "albumId",
        onDelete = ForeignKey.CASCADE))
public class Movie {
    @PrimaryKey(autoGenerate = true)
    private int movieId;
    private int albumId;
    private String dataUrl;
    private String movieTitle;
    private String description;
    private String details;
    private String duration;
    private String singers;
    private String musicDirector;
    private String coverUrl;
    private Boolean favorit = false;

    public long getFavoritMarkedAt() {
        return favoritMarkedAt;
    }

    public void setFavoritMarkedAt(long favoritMarkedAt) {
        this.favoritMarkedAt = favoritMarkedAt;
    }

    private long favoritMarkedAt;

    public Boolean isFavorit() {
        return favorit;
    }

    public void setFavorit(Boolean favorit) {
        this.favorit = favorit;
    }

    public Movie(String dataUrl, String movieTitle, String description) {
        this.dataUrl = dataUrl;
        this.movieTitle = movieTitle;
        this.description = description;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSingers() {
        return singers;
    }

    public void setSingers(String singers) {
        this.singers = singers;
    }

    public String getDataUrl() {
        return dataUrl;
    }

    public void setDataUrl(String dataUrl) {
        this.dataUrl = dataUrl;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isValid() {
        return !(dataUrl == null || dataUrl.length() == 0);
    }

    public void setMusicDirector(String musicDirector) {
        this.musicDirector = musicDirector;
    }

    public String getMusicDirector() {
        return musicDirector;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getCoverUrl() {
        return coverUrl;
    }
}
