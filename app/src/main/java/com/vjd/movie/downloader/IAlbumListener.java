package com.vjd.movie.downloader;

import com.vjd.movie.downloader.models.Movie;
import com.vjd.movie.downloader.models.MovieBasic;

public interface IAlbumListener {
    public void onClick(int position, MovieBasic movieBasic);
}