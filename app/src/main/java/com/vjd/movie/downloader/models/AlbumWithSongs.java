package com.vjd.movie.downloader.models;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class AlbumWithSongs {
    @Embedded
    public Album album;
    @Relation(parentColumn = "id", entityColumn = "albumId", entity = Movie.class)
    public List<Movie> movieList;

    public List<Movie> getMovieList() {
        return movieList;
    }

    public void setMovieList(List<Movie> movieList) {
        this.movieList = movieList;
    }
}
