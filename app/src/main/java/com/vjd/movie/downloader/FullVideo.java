package com.vjd.movie.downloader;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import tcking.github.com.giraffeplayer2.GiraffePlayer;
import tcking.github.com.giraffeplayer2.PlayerListener;
import tv.danmaku.ijk.media.player.IjkTimedText;

import static com.vjd.movie.downloader.Constants.VIDEO_LINK;

public class FullVideo extends AppCompatActivity {

    ProgressDialog dialog;
    VideoView videoview;
    private int position = 0;
    String videoUrl, imageUrl, cid;
    private static final String TAG = "FullVideo";
    WebView myWebView;
    private OkHttpClient client = null;
    private int cacheSize = 20 * 1024 * 1024; // 20MB

    private MediaController mediaControls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        client = new OkHttpClient.Builder().cache(new Cache(getCacheDir(), cacheSize)).build();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_full_video);

        dialog = new ProgressDialog(this);
        dialog.setMessage("Buffering..");
        dialog.show();

        if (getIntent().hasExtra(VIDEO_LINK)) {
            videoUrl = getIntent().getStringExtra(VIDEO_LINK);
        } else {// demo link
            videoUrl = "https://woof.tube/gettoken/NosJjhae9jJ~1566890287~110.39.0.0~9gvfKsJx";
        }

        if (mediaControls == null) {
            mediaControls = new MediaController(this);
        }

        myWebView = findViewById(R.id.webview);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportMultipleWindows(true);
        myWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        myWebView.setWebViewClient(new MyWebViewClient());


        //downloadMoviesHomePage("https://azm.to/movie/Fast+%26+Furious+Presents%3A+Hobbs+%26+Shaw");


        WebChromeClient webChromeClient = new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                result.cancel();
                return true;
            }

            @Override
            public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
                result.cancel();
                return true;
            }

            @Override
            public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
                result.cancel();
                return true;
            }
        };

        myWebView.setWebChromeClient(webChromeClient);
        myWebView.loadUrl(videoUrl);

        /* */


    }

    String htmlObtained = null;

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {


            return false;


        }

        @Override
        public void onPageFinished(WebView view, String url) {

         /*   myWebView.loadUrl("javascript:window.HtmlViewer.showHTML" +
                    "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");*/
            Handler handler = new Handler();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {

                    myWebView.evaluateJavascript(
                            "(function() { return (document.getElementsByTagName('video')[0].innerHTML); })();",
                            html -> {
                                if (html != null) {

                                    myWebView.setVisibility(View.GONE);
                                    String url1 = html.substring(html.indexOf("src") + 6, html.indexOf("type=") - 3).replace("amp;", "");
                                    html.substring(html.indexOf("src") + 6, html.indexOf("type=") - 3).replace("amp;", "");

                                    FullVideo.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tcking.github.com.giraffeplayer2.VideoView videoView = findViewById(R.id.video_view);
                                            videoView.setVideoPath(url1).getPlayer().start();
                                            videoView.setVisibility(View.VISIBLE);

                                            videoView.setPlayerListener(new PlayerListener() {
                                                @Override
                                                public void onPrepared(GiraffePlayer giraffePlayer) {
                                                    Log.d(TAG, "onPrepared: Prepared");
                                                }

                                                @Override
                                                public void onBufferingUpdate(GiraffePlayer giraffePlayer, int percent) {
                                                    Log.d(TAG, "onBufferingUpdate: Buddering");
                                                }

                                                @Override
                                                public boolean onInfo(GiraffePlayer giraffePlayer, int what, int extra) {
                                                    Log.d(TAG, "onInfo: ");
                                                    if (dialog.isShowing()) {
                                                        dialog.dismiss();
                                                    }
                                                    return false;
                                                }

                                                @Override
                                                public void onCompletion(GiraffePlayer giraffePlayer) {
                                                    Log.d(TAG, "onCompletion: Complete");
                                                }

                                                @Override
                                                public void onSeekComplete(GiraffePlayer giraffePlayer) {

                                                }

                                                @Override
                                                public boolean onError(GiraffePlayer giraffePlayer, int what, int extra) {
                                                    return false;
                                                }

                                                @Override
                                                public void onPause(GiraffePlayer giraffePlayer) {
                                                    Log.d(TAG, "onPause: ");
                                                }

                                                @Override
                                                public void onRelease(GiraffePlayer giraffePlayer) {

                                                }

                                                @Override
                                                public void onStart(GiraffePlayer giraffePlayer) {
                                                    Log.d(TAG, "onStart: ");
                                                }

                                                @Override
                                                public void onTargetStateChange(int oldState, int newState) {
                                                    Log.d(TAG, "onTargetStateChange: ");
                                                }

                                                @Override
                                                public void onCurrentStateChange(int oldState, int newState) {

                                                }

                                                @Override
                                                public void onDisplayModelChange(int oldModel, int newModel) {

                                                }

                                                @Override
                                                public void onPreparing(GiraffePlayer giraffePlayer) {

                                                }

                                                @Override
                                                public void onTimedText(GiraffePlayer giraffePlayer, IjkTimedText text) {

                                                }

                                                @Override
                                                public void onLazyLoadProgress(GiraffePlayer giraffePlayer, int progress) {

                                                }

                                                @Override
                                                public void onLazyLoadError(GiraffePlayer giraffePlayer, String message) {

                                                }
                                            });
                                            Log.d("HTML", html);
                                            // code here

                                        }
                                    });
                                }
                                handler.removeCallbacks(this::run);
                            });


                }
            };
            handler.postDelayed(runnable,4000);


        }


        @SuppressLint("NewApi")
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
            Log.d(TAG, "request.getRequestHeaders()::" + request.getRequestHeaders());
            Log.d(TAG, "shouldInterceptRequest: " + request.getUrl());

            return null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void downloadMoviesHomePage(String url) {

        Request request = new Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(TAG, "onFailure: " + e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String page = response.body().string();
                Document doc = Jsoup.parse(page);

                Elements iframe = doc.select("ul[id=serverul]>li");

                Element singleServer = iframe.get(0);

                String url = "https://azm.to" + singleServer.select("a").attr("href");


                runOnUiThread(() -> {



                  /*  tcking.github.com.giraffeplayer2.VideoView videoView = findViewById(R.id.video_view);
                    videoView.setVideoPath("https://sub2.azmovie.to/It.Chapter.Two.2019.KORSUB.HDRip.x264-STUTTERSHIT.mp4?h=hww-Vbx0Hk1QeJ5_0EE0vg&t=1571999777&p=5e056c").getPlayer().start();
*/
                });

            }
        });
    }


}