package com.vjd.movie.downloader;

import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.vjd.movie.downloader.adapters.MoviesAdapter;
import com.vjd.movie.downloader.adapters.TrendingMoviesAdapter;
import com.vjd.movie.downloader.adapters.YearsCardAdapter;
import com.vjd.movie.downloader.databinding.ActivityMainBinding;
import com.vjd.movie.downloader.models.Country;
import com.vjd.movie.downloader.models.Genre;
import com.vjd.movie.downloader.models.MovieBasic;
import com.vjd.movie.downloader.models.Tag;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends DaggerAppCompatActivity {

    private OkHttpClient client = null;
    private int cacheSize = 20 * 1024 * 1024; // 20MB
    private static final String TAG = "MainActivity";
    private ActivityMainBinding binding;
    private List<Country> countries;
    private AlertDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(binding.toolbarLayout);
        client = new OkHttpClient.Builder().cache(new Cache(getCacheDir(), cacheSize)).build();

        binding.shimmerViewContainer.startShimmerAnimation();
        binding.shimmerViewContainer2.startShimmerAnimation();
        binding.shimmerViewContainer3.startShimmerAnimation();

        loadingDialog = new AlertDialog.Builder(this)
                .setTitle("Hold On")
                .setMessage("Loading Movies Data")
                .setIcon(android.R.drawable.alert_dark_frame)
                .show();

        downloadMoviesHomePage(Constants.URL_BASE_MOVIES, movieBasics -> {
            if (loadingDialog != null && loadingDialog.isShowing()) {
                loadingDialog.dismiss();
            }
            runOnUiThread(() -> {
                TrendingMoviesAdapter trendingMoviesAdapter = new TrendingMoviesAdapter(MainActivity.this);
                binding.rvTrendingMovies.setAdapter(trendingMoviesAdapter);
                binding.rvTrendingMovies.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                LinearSnapHelper linearSnapHelper = new SnapHelperOneByOne();
                linearSnapHelper.attachToRecyclerView(binding.rvTrendingMovies);
                binding.indicator.attachToRecyclerView(binding.rvTrendingMovies, 1);
                trendingMoviesAdapter.setAlbumList(movieBasics);
                binding.shimmerViewContainer.stopShimmerAnimation();
                binding.shimmerViewContainer.setVisibility(View.GONE);
                trendingMoviesAdapter.setAlbumListener((position, movieBasic) -> {
                    Intent intent = new Intent(MainActivity.this, MovieDetailsActivity.class);
                    intent.putExtra("MOVIE", movieBasics.get(position));
                    startActivity(intent);
                });
            });

        }, genres -> {
            if (genres.size() > 0) {
                runOnUiThread(() -> {
                    ArrayList<Chip> chipList = new ArrayList<>();
                    for (Genre genre : genres
                    ) {
                        chipList.add(new Tag(genre.getTitle()) {
                        });
                    }
                    ChipView chipDefault = findViewById(R.id.chipview);
                    chipDefault.setChipLayoutRes(R.layout.item_chip_view);
                    chipDefault.setChipList(chipList);
                    chipDefault.setOnChipClickListener((Chip chip) -> {

                    });
                });
            }
        }, movieBasics -> {
            runOnUiThread(() -> {
                MoviesAdapter moviesAdapter = new MoviesAdapter(MainActivity.this);
                binding.rvRecentMovies.setAdapter(moviesAdapter);

                GridLayoutManager layoutManager =
                        new GridLayoutManager(MainActivity.this, 2, GridLayoutManager.HORIZONTAL, false);

                binding.rvRecentMovies.setLayoutManager(layoutManager);
                binding.shimmerViewContainer2.stopShimmerAnimation();
                binding.shimmerViewContainer2.setVisibility(View.GONE);
                moviesAdapter.setAlbumList(movieBasics);
                moviesAdapter.setAlbumListener((position, movieBasic) -> {
                    Intent intent = new Intent(MainActivity.this, MovieDetailsActivity.class);
                    intent.putExtra("MOVIE", movieBasics.get(position));
                    startActivity(intent);
                });
            });
        }, movieBasics -> {
            runOnUiThread(() -> {
                MoviesAdapter moviesAdapter = new MoviesAdapter(MainActivity.this);

                binding.rvFeaturedMovies.setAdapter(moviesAdapter);
                GridLayoutManager layoutManager =
                        new GridLayoutManager(MainActivity.this, 2, GridLayoutManager.HORIZONTAL, false);
                binding.rvFeaturedMovies.setLayoutManager(layoutManager);


                binding.shimmerViewContainer3.stopShimmerAnimation();
                binding.shimmerViewContainer3.setVisibility(View.GONE);
                moviesAdapter.setAlbumList(movieBasics);
                moviesAdapter.setAlbumListener((position, movieBasic) -> {
                    Intent intent = new Intent(MainActivity.this, MovieDetailsActivity.class);
                    intent.putExtra("MOVIE", movieBasics.get(position));
                    startActivity(intent);
                });
            });
        }, years -> runOnUiThread(() -> {
            YearsCardAdapter moviesAdapter = new YearsCardAdapter(MainActivity.this,  years.subList(0,16));
            binding.rvYears.setAdapter(moviesAdapter);
        }));


        NavigationFragment navigationFragment = new NavigationFragment();
        navigationFragment.setup(binding.drawerLayout, binding.toolbarLayout);
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        binding.hamburgerIcon.setOnClickListener(v -> {
            if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                binding.drawerLayout.closeDrawers();
            } else {
                binding.drawerLayout.openDrawer(GravityCompat.START);
            }
        });

    }


    private void downloadMoviesHomePage(String url, onMoviesListFetched dataListener, onGenresListFetched genresListFetched, onRecentMoviesListFetched recentMoviesListFetched, onFeaturedMoviesListFetched featuredMoviesListFetched,onYearsFetched yearsFetched) {

        Request request = new Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(TAG, "onFailure: " + e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String page = response.body().string();
                Document doc = Jsoup.parse(page);

                Elements moviesList = doc.select("body>div>a");

                List<MovieBasic> listMovies = Observable.fromIterable(moviesList)
                        .subscribeOn(Schedulers.newThread())
                        .map(MainActivity.this::processHomePageMovies)
                        .toList().blockingGet();
                dataListener.processMoviesList(listMovies);

                Elements genresList = doc.select("li.genre>ul[class=amber lighten-1 z-depth-2 subnav genre]>li");

                List<Genre> listGenres = Observable.fromIterable(genresList)
                        .subscribeOn(Schedulers.newThread())
                        .map(MainActivity.this::processHomePageGenres)
                        .toList().blockingGet();
                genresListFetched.processGenresList(listGenres);

                Elements recentlyAddedMovies = doc.select("div[class=row section more]>div.grid-image");

                List<MovieBasic> recentlyAddedMoviesList = Observable.fromIterable(recentlyAddedMovies)
                        .subscribeOn(Schedulers.newThread())
                        .map(MainActivity.this::processRecentlyAddedMovies)
                        .toList().blockingGet();
                recentMoviesListFetched.processRecentMoviesList(recentlyAddedMoviesList);

                Elements featuredMovies = doc.select("div[class=row section]>div.grid-image");

                List<MovieBasic> featuredMoviesList = Observable.fromIterable(featuredMovies)
                        .subscribeOn(Schedulers.newThread())
                        .map(MainActivity.this::processRecentlyAddedMovies)
                        .toList().blockingGet();
                featuredMoviesListFetched.processFeaturedMoviesList(featuredMoviesList);


                Elements yearsList = doc.select("li.genre>ul[class=amber lighten-1 z-depth-1 subnav genre]>li");
                List<Genre> listYears = Observable.fromIterable(yearsList)
                        .subscribeOn(Schedulers.newThread())
                        .map(MainActivity.this::processHomePageGenres)
                        .toList().blockingGet();
                yearsFetched.processyearsData((ArrayList<Genre>) listYears);


            }
        });
    }


    interface onMoviesListFetched {
        void processMoviesList(List<MovieBasic> movieBasics);
    }

    interface onGenresListFetched {
        void processGenresList(List<Genre> genres);
    }

    interface onRecentMoviesListFetched {
        void processRecentMoviesList(List<MovieBasic> movieBasics);
    }

    interface onFeaturedMoviesListFetched {
        void processFeaturedMoviesList(List<MovieBasic> movieBasics);
    }

    interface onYearsFetched {
        void processyearsData(ArrayList<Genre> years);
    }


    public MovieBasic processHomePageMovies(Element movieElement) {

        Log.d(TAG, "processCountriesList: processing item on thread " + Thread.currentThread().getName());

        String movieTitle = movieElement.select("img").attr("title"); //  // Movie name
        String detailsLink = movieElement.attr("href");//  // Movie link
        String thumbnailUrl = movieElement.select("img").attr("src");//  // Movie thumbNail
        MovieBasic movieBasic = new MovieBasic(movieTitle, thumbnailUrl, detailsLink);

        return movieBasic;
    }

    public Genre processHomePageGenres(Element genreElement) {

        Log.d(TAG, "processGenresList: processing item on thread " + Thread.currentThread().getName());

        String genreLink = genreElement.select("a").attr("href"); //  // Genre title
        String genreTitle = genreElement.select("a").text();                //    // Genre link
        Genre genre = new Genre(genreTitle, genreLink);

        return genre;
    }

    public MovieBasic processRecentlyAddedMovies(Element movieElement) {

        Log.d(TAG, "processGenresList: processing item on thread " + Thread.currentThread().getName());

        String movieTitle = movieElement.select("a>img").attr("alt"); //  // Movie name
        String detailsLink = movieElement.select("a").attr("href");//  // Movie link
        String thumbnailUrl = movieElement.select("a>img").attr("data-src");//  // Movie thumbNail
        String quality = movieElement.select("div[class=caption]>a>div[class=info]>span").first().text();//  // Movie quality
        String rating = movieElement.select("div[class=caption]>a>div[class=info]>span.imdb").text();//  // Movie Rating
        MovieBasic movieBasic = new MovieBasic(movieTitle, thumbnailUrl, detailsLink, quality, rating);

        return movieBasic;
    }

}
