package com.vjd.movie.downloader.di;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.vjd.movie.downloader.di.viewmodel.ViewModelKey;
import com.vjd.movie.downloader.di.viewmodel.ViewModelProviderFactory;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;


@Module
public abstract class ViewModelFactoryModule {

    @Binds
    public abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelProviderFactory viewModelProviderFactory);


  /*  @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindAuthViewModel(MainViewModel mainViewModel);*/


}