package com.vjd.movie.downloader.models;

import android.util.Log;

public class MovieCast {
    private String director;
    private String writer;
    private String starring;
    private String genres;
    private String country;
    private String language;

    public MovieCast() {
        String key = "none";
        this.director = key;
        this.writer = key;
        this.starring = key;
        this.genres = key;
        this.country = key;
        this.language = key;
    }

    public MovieCast(String castDetails) {
        String[] tempArray;
        String delimiter = "\n";
        tempArray = castDetails.split(delimiter);

        for (int i = 0; i < tempArray.length; i++) {
            String key = tempArray[i];
            if (key.contains("Directed by:")) {
                this.director = key;
            } else if (key.contains("Written by:")) {
                this.writer = key;
            } else if (key.contains("Starring by:")) {
                this.starring = key;
            } else if (key.contains("Genres:")) {
                this.genres = key;
            } else if (key.contains("Country:")) {
                this.country = key;
            } else if (key.contains("Language:")) {
                this.language = key;
            }
            Log.e("allLink", "values: " + key);
        }
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getStarring() {
        return starring;
    }

    public void setStarring(String starring) {
        this.starring = starring;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
