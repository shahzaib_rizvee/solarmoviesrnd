package com.vjd.movie.downloader.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.vjd.movie.downloader.models.Movie;

import java.util.List;


@Dao
public abstract class SongDao extends BaseDao<Movie> {

    @Query("select * from Movie where favorit = 1 order by favoritMarkedAt DESC")
    public abstract LiveData<List<Movie>> getFavoritSongLiveData();

    @Query("select * from Movie where favorit = 1 order by favoritMarkedAt")
    public abstract List<Movie> getFavoritSong();
}