package com.vjd.movie.downloader.di;


import com.vjd.movie.downloader.MainActivity;
import com.vjd.movie.downloader.MovieDetailsActivity;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModule {

    @ContributesAndroidInjector
    abstract MainActivity contributeMainActivity();

    @ContributesAndroidInjector
    abstract MovieDetailsActivity contributeMovieDetailsActivity();

    @Provides
    static String firstInjectionString() {
        return "My first Injection String";
    }
}
