package com.vjd.movie.downloader.models;

public class Country {

    private String countryName;
    private String link;

    public Country(String countryName, String link) {
        this.countryName = countryName;
        this.link = link;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getLink() {
        return link;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public void setLink(String link) {
        this.link = link;
    }

}
