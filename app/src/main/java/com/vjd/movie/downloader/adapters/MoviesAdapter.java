package com.vjd.movie.downloader.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.vjd.movie.downloader.R;
import com.vjd.movie.downloader.databinding.ItemviewMovieCardBinding;
import com.vjd.movie.downloader.models.MovieBasic;

public class MoviesAdapter extends MoviesBaseAdapter {

    private Context mContext;

    public MoviesAdapter(Context context) {
        mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.itemview_movie_card, parent, false);
        return new AlbumVerticalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        final MovieBasic movieBasic = movieBasics.get(position);
        AlbumVerticalViewHolder albumVerticalViewHolder = (AlbumVerticalViewHolder) holder;
        albumVerticalViewHolder.binding.setMovie(movieBasic);
        albumVerticalViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iAlbumListener != null)
                    iAlbumListener.onClick(position, movieBasic);
            }
        });
        Glide.with(mContext).load(movieBasic.getThumbnailUrl()).into(
                albumVerticalViewHolder.binding.simpleDraweeViewItemMoviePhoto);

    }

    public class AlbumVerticalViewHolder extends RecyclerView.ViewHolder {
        ItemviewMovieCardBinding binding;

        public AlbumVerticalViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
