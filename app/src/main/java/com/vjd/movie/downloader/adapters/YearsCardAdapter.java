package com.vjd.movie.downloader.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vjd.movie.downloader.R;
import com.vjd.movie.downloader.models.Genre;

import java.util.List;

public class YearsCardAdapter extends RecyclerView.Adapter<YearsCardAdapter.YearViewHodler> {

    private Context mContext;
    private List<Genre> years;

    public YearsCardAdapter(Context context, List<Genre> years) {
        mContext = context;
        this.years = years;
    }

    @NonNull
    @Override
    public YearViewHodler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.itemview_year, parent, false);
        return new YearViewHodler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull YearViewHodler holder, int position) {

        final Genre year = years.get(position);
        YearViewHodler albumVerticalViewHolder = (YearViewHodler) holder;
        holder.year.setText(year.getTitle());
        albumVerticalViewHolder.itemView.setOnClickListener(v -> {

        });


    }

    @Override
    public int getItemCount() {
        return years.size();
    }

    public class YearViewHodler extends RecyclerView.ViewHolder {

        TextView year;

        public YearViewHodler(@NonNull View itemView) {
            super(itemView);
            year = itemView.findViewById(R.id.tv_year);
        }
    }

}

