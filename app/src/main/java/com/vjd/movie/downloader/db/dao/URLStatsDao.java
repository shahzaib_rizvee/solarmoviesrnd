package com.vjd.movie.downloader.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;


import com.vjd.movie.downloader.models.URLStats;

import java.util.List;

@Dao
public abstract class URLStatsDao extends BaseDao<URLStats> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract long insert(URLStats obj);

    @Query("SELECT * FROM URLStats ")
    public abstract List<URLStats> findAll();
}