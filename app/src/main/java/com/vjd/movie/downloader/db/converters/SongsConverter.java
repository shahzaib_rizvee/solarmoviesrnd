package com.vjd.movie.downloader.db.converters;

import androidx.room.TypeConverter;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;
import com.vjd.movie.downloader.models.Movie;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

public class SongsConverter {

    @TypeConverter
    public String fromPlanResultsList(List<Movie> planResults) {
        Moshi moshi = new Moshi.Builder().build();
        Type type = Types.newParameterizedType(List.class, Movie.class);
        JsonAdapter<List<Movie>> listJsonAdapter = moshi.adapter(type);
        String json = listJsonAdapter.toJson(planResults);
        return json;
    }

    @TypeConverter
    public List<Movie> fromString(String planResults) {
        Moshi moshi = new Moshi.Builder().build();
        Type type = Types.newParameterizedType(List.class, Movie.class);
        JsonAdapter<List<Movie>> listJsonAdapter = moshi.adapter(type);
        try {
            return listJsonAdapter.fromJson(planResults);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
