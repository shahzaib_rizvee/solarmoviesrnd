package com.vjd.movie.downloader;

import android.content.Context;
import android.os.Build;
import android.view.View;


import com.facebook.drawee.backends.pipeline.Fresco;
import com.vjd.movie.downloader.di.AppComponent;
import com.vjd.movie.downloader.di.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class BaseApplication extends DaggerApplication {

    private static BaseApplication instance;

    public static BaseApplication getInstance() {
        return instance;
    }

    public static Context APPLICATION_CONTEXT;

    public static Context getContext() {
        return APPLICATION_CONTEXT;
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().applicattion(this).build();
        appComponent.inject(this);
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
      /*  Fabric.with(this, new Crashlytics());

        Stetho.initializeWithDefaults(this);*/

        instance = this;
        APPLICATION_CONTEXT = getInstance().getApplicationContext();
        Fresco.initialize(this);

       /* if (BuildConfig.DEBUG) {
            //   Timber.plant(Timber.DebugTree())
            Stetho.initializeWithDefaults(this);
        }
        AdsManager.getInstance();*/
    }
}
