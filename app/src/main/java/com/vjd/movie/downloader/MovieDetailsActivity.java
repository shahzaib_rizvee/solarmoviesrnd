package com.vjd.movie.downloader;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.Glide;
import com.vjd.movie.downloader.adapters.MoviesAdapter;
import com.vjd.movie.downloader.databinding.ActivityMovieDetailsBinding;
import com.vjd.movie.downloader.models.MovieBasic;
import com.vjd.movie.downloader.models.MovieDetails;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.vjd.movie.downloader.Constants.VIDEO_LINK;

public class MovieDetailsActivity extends AppCompatActivity {

    private static final String TAG = "MovieDetailsActivity";
    private OkHttpClient client = null;
    private int cacheSize = 20 * 1024 * 1024; // 20MB
    private MovieBasic movieBasic;
    private ActivityMovieDetailsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_movie_details);
        movieBasic = getIntent().getParcelableExtra("MOVIE");
        binding.shimmerViewContainer.startShimmerAnimation();
        client = new OkHttpClient.Builder().cache(new Cache(getCacheDir(), cacheSize)).build();
        Glide.with(this).load(movieBasic.getThumbnailUrl()).into(binding.simpleDraweeViewItemMoviePhoto);
        Glide.with(this).load(movieBasic.getThumbnailUrl()).into(binding.simpleDraweeViewItemMoviePoster);
        downloadMoviesHomePage(Constants.URL_BASE_MOVIES + movieBasic.getDetailsUrl(), link -> {
            if (link.size() > 0) {
                runOnUiThread(() -> binding.watchLayout.setOnClickListener(v -> {
                    startNextActivity(Constants.URL_BASE_MOVIES + link.get(0));
                }));
            }
        }, details -> runOnUiThread(() -> {
                    if (details.size() > 0) {
                        binding.setMovie(details.get(0));
                    }
                }
        ), movieBasics -> {
            runOnUiThread(() -> {
                MoviesAdapter moviesAdapter = new MoviesAdapter(MovieDetailsActivity.this);
                binding.rvRelatedMovies.setAdapter(moviesAdapter);

                GridLayoutManager layoutManager =
                        new GridLayoutManager(MovieDetailsActivity.this, 2, GridLayoutManager.HORIZONTAL, false);

                binding.rvRelatedMovies.setLayoutManager(layoutManager);
                binding.shimmerViewContainer.stopShimmerAnimation();
                binding.shimmerViewContainer.setVisibility(View.GONE);
                moviesAdapter.setAlbumList(movieBasics);
                moviesAdapter.setAlbumListener((position, movieBasic) -> {
                    Intent intent = new Intent(MovieDetailsActivity.this, MovieDetailsActivity.class);
                    intent.putExtra("MOVIE", movieBasics.get(position));
                    startActivity(intent);
                });
            });
        });
    }

    public void startNextActivity(String streamingLink) {
        Intent intent = new Intent(MovieDetailsActivity.this, FullVideo.class);
        intent.putExtra(VIDEO_LINK, streamingLink);
        startActivity(intent);
        finish();
    }

    interface onRelatedMoviesListFetched {
        void processRelatedMoviesList(List<MovieBasic> movieBasics);
    }

    private void downloadMoviesHomePage(String url, onFullScreenLinkFetched dataListener2, onMovieDetailsFetched movieDetailsFetched, onRelatedMoviesListFetched relatedMoviesListFetched) {

        Request request = new Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(TAG, "onFailure: " + e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String page = response.body().string();
                Document doc = Jsoup.parse(page);

                Elements moviesList = doc.select("div.player");

                List<String> movieFullScreenLink = Observable.fromIterable(moviesList)
                        .subscribeOn(Schedulers.newThread())
                        .map(MovieDetailsActivity.this::processHomePageMovies)
                        .toList().blockingGet();
                dataListener2.processCountriesList(movieFullScreenLink);

                Elements movieDetails = doc.select("div[class=col 12 white-text left-align]");

                List<MovieDetails> movieDetailsList = Observable.fromIterable(movieDetails)
                        .subscribeOn(Schedulers.newThread())
                        .map(MovieDetailsActivity.this::processMovieDetails)
                        .toList().blockingGet();
                movieDetailsFetched.processMovieDetails(movieDetailsList);

                Elements recentlyAddedMovies = doc.select("div[class=grid-image]");

                List<MovieBasic> recentlyAddedMoviesList = Observable.fromIterable(recentlyAddedMovies)
                        .subscribeOn(Schedulers.newThread())
                        .map(MovieDetailsActivity.this::processRelatedMovies)
                        .toList().blockingGet();
                relatedMoviesListFetched.processRelatedMoviesList(recentlyAddedMoviesList);

            }
        });
    }

    public String processHomePageMovies(Element movieElement) {

        Log.d(TAG, "processCountriesList: processing item on thread " + Thread.currentThread().getName());

        String movieFullScreenLink = movieElement.select("div:last-child>iframe").attr("src"); //  // country name


        return movieFullScreenLink;
    }


    public MovieDetails processMovieDetails(Element movieElement) {

        Log.d(TAG, "processMovieDetails: processing item on thread " + Thread.currentThread().getName());

        String movieRuntime = movieElement.select("p>span[itemprop=duration]").text();

        Elements allGenres = movieElement.select("p>a");
        String generesString = "";
        for (Element e : allGenres
        ) {
            generesString = generesString.concat(e.text() + ",");
        }
        generesString = generesString.substring(0, generesString.length() - 2);

        String movieDirectors = movieElement.select("p>span[itemprop = director]").text();

        String movieRating = movieElement.select("p>span[itemprop = aggregateRating]").text();

        String movieQuality = movieElement.select("p>span[itemprop = quality imdblogo amber lighten-1]").text();

        String movieDescription = movieElement.select("div.plot").text();

        String movieRelease = movieElement.select("p>span[itemprop=duration]").text();

        String actors = movieElement.select("p>span[itemprop=duration]").text();

        movieBasic.setRating(movieRating);

        movieBasic.setQuality(movieQuality);

        MovieDetails movieDetails = new MovieDetails(movieBasic, generesString, movieDirectors, movieDescription, movieRuntime, actors, movieRelease);

        return movieDetails;
    }

    interface onFullScreenLinkFetched {
        void processCountriesList(List<String> link);
    }

    interface onMovieDetailsFetched {
        void processMovieDetails(List<MovieDetails> details);
    }

    public MovieBasic processRelatedMovies(Element movieElement) {

        Log.d(TAG, "processRelatedMovies: processing item on thread " + Thread.currentThread().getName());
        String movieTitle = movieElement.select("a>img").attr("alt"); //  // Movie name
        String detailsLink = movieElement.select("a").attr("href");//  // Movie link
        String thumbnailUrl = movieElement.select("a>img").attr("data-src");//  // Movie thumbNail
        String quality = movieElement.select("div[class=caption]>a>div[class=info]>span").first().text();//  // Movie quality
        String rating = movieElement.select("div[class=caption]>a>div[class=info]>span.imdb").text();//  // Movie Rating
        MovieBasic movieBasic = new MovieBasic(movieTitle, thumbnailUrl, detailsLink, quality, rating);
        return movieBasic;

    }
}
