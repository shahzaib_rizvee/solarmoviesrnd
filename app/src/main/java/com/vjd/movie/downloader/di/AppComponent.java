package com.vjd.movie.downloader.di;

import android.app.Application;


import com.vjd.movie.downloader.BaseApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;


@Singleton
@Component(
        modules = {
                AndroidSupportInjectionModule.class,
                ActivityBuilderModule.class, ViewModelFactoryModule.class,
                AppModule.class, NetworkModule.class, DbModule.class
        }
)
public interface AppComponent extends AndroidInjector<BaseApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder applicattion(Application application);

        AppComponent build();
    }
}
