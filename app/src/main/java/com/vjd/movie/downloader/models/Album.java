package com.vjd.movie.downloader.models;

import android.util.Log;

import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.ArrayList;
import java.util.List;

@Entity(indices = {@Index("id")})
public class Album {
    @Ignore
    private static final String TAG = "Album";

    @PrimaryKey(autoGenerate = true)
    private Integer id;

    private String photoUri;
    private String title;
    private String subTitle;
    private String detailsLink;
    private List<String> tags;
    private List<String> musicDirector;
    private String released;
    private String cast;
    private String lyricist;
    private String duration;
    private String singers;
    // private List<Movie> songsList;

//    public List<Movie> getSongsList() {
//        return songsList;
//    }
//
//    public void setSongsList(List<Movie> songsList) {
//        this.songsList = songsList;
//    }


    public List<String> getMusicDirector() {
        return musicDirector;
    }

    public void setMusicDirector(List<String> musicDirector) {
        this.musicDirector = musicDirector;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getLyricist() {
        return lyricist;
    }

    public void setLyricist(String lyricist) {
        this.lyricist = lyricist;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSingers() {
        return singers;
    }

    public void setSingers(String singers) {
        this.singers = singers;
    }

    public Album() {
    }


    @Ignore
    public Album(String title, String singarName, String photoUri) {
        this.photoUri = photoUri;
        this.title = title;
        this.subTitle = singarName;

    }

    @Ignore
    public Album(String title, String singarName, String photoUri, String detailsLink) {
        this.photoUri = photoUri;
        this.title = title;
        this.subTitle = singarName;
        this.detailsLink = detailsLink;
        // this.id = extractId();
        tags = new ArrayList<>();
        musicDirector = new ArrayList<>();
        //  songsList = new ArrayList<>();

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setDetailsLink(String detailsLink) {
        this.detailsLink = detailsLink;
    }

    public String getDetailsLink() {
        return detailsLink;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(String photoUri) {
        this.photoUri = photoUri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }


    private int extractId() {
        Log.d(TAG, "extractId from: " + photoUri);
        int slashIndex = photoUri.lastIndexOf('/');
        int dotLocation = photoUri.lastIndexOf('.');
        String idString = photoUri.substring(slashIndex + 1, dotLocation);
        int id = 1;
        try {
            id = Integer.parseInt(idString);
        } catch (Exception e) {
            Log.e(TAG, "missed ID: ", e);
        }
        return id;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void addTag(String tag) {
        this.tags.add(tag);
    }

//    public void addSong(Movie song) {
//        songsList.add(song);
//    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return true;
    }
}
