package com.vjd.movie.downloader.models;

import android.os.Parcel;
import android.os.Parcelable;

public class MovieBasic implements Parcelable {

    private String title;
    private String thumbnailUrl;
    private String detailsUrl;
    private String quality;
    private String rating;

    public MovieBasic(String title, String thumbnailUrl, String detailsUrl) {
        this.title = title;
        this.thumbnailUrl = thumbnailUrl;
        this.detailsUrl = detailsUrl;
    }

    protected MovieBasic(Parcel in) {
        title = in.readString();
        thumbnailUrl = in.readString();
        detailsUrl = in.readString();
        quality = in.readString();
        rating = in.readString();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getDetailsUrl() {
        return detailsUrl;
    }

    public void setDetailsUrl(String detailsUrl) {
        this.detailsUrl = detailsUrl;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }


    public static final Creator<MovieBasic> CREATOR = new Creator<MovieBasic>() {
        @Override
        public MovieBasic createFromParcel(Parcel in) {
            return new MovieBasic(in);
        }

        @Override
        public MovieBasic[] newArray(int size) {
            return new MovieBasic[size];
        }
    };

    public MovieBasic(String title, String thumbnailUrl, String detailsUrl, String quality, String rating) {
        this.title = title;
        this.thumbnailUrl = thumbnailUrl;
        this.detailsUrl = detailsUrl;
        this.quality = quality;
        this.rating = rating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(thumbnailUrl);
        dest.writeString(detailsUrl);
        dest.writeString(quality);
        dest.writeString(rating);
    }

}
