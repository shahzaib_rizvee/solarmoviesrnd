package com.vjd.movie.downloader.models;

import java.io.File;
import java.text.DecimalFormat;

public class ItemDownload implements Cloneable {
    int pos;
    String name;
    String foldername;
    String fullPath;
    long size;
    File file;

    public ItemDownload() {
        this.pos = 0;
        this.name = "";
        this.foldername = "";
        this.fullPath = "";
        this.file = null;
        this.size = 0L;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        // https://www.journaldev.com/60/java-clone-object-cloning-java
        return super.clone();
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFoldername() {
        return foldername;
    }

    public void setFoldername(String foldername) {
        this.foldername = foldername;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public long getType() {
        return size;
    }

    public void setType(long type) {
        this.size = type;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public long getSize() {
        return size;
    }

    public String getFileSizeInUnits() {
        return readableFileSize(this.size);
    }

    public void setSize(long size) {
        this.size = size;
    }

    public static String readableFileSize(long size) {
        if (size <= 0) return "0";
        final String[] units = new String[]{"B", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }
}
