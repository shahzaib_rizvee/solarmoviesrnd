package com.vjd.movie.downloader.di;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.room.Room;

import com.vjd.movie.downloader.db.AppDatabase;
import com.vjd.movie.downloader.db.dao.AlbumDao;
import com.vjd.movie.downloader.db.dao.SongDao;
import com.vjd.movie.downloader.db.dao.URLStatsDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DbModule {

    @Provides
    @Singleton
    AppDatabase provideDatabase(Application application) {
        return Room.databaseBuilder(application,
                AppDatabase.class, "music.db")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries().build();
    }

    @Provides
    @Singleton
    SongDao provideSongDao(@NonNull AppDatabase appDatabase) {
        return appDatabase.getSongDao();
    }

    @Provides
    @Singleton
    AlbumDao provideAlbumDao(@NonNull AppDatabase appDatabase) {
        return appDatabase.getAlbumDao();
    }


    @Provides
    @Singleton
    URLStatsDao provideURLStatsDao(@NonNull AppDatabase appDatabase) {
        return appDatabase.getURLStatsDao();
    }

}